package ar.edu.unq.po2.tp7.SueldosRecargados;

public abstract class Empleado {
	
	private Double sueldoBasico;
	private String nombre;
	private String apellido;
	private int hijos;
	private Boolean esCasado;
	private int horasTrabajadas;
	private int retencion;

	public Empleado(Double sueldoBasico, String nombre, String apellido, int hijos, Boolean esCasado, int horas, int retencion) {
		
		this.setSueldoBasico(sueldoBasico);
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setHijos(hijos);
		this.setEsCasado(esCasado);
		this.setHorasTrabajadas(horas);
		this.setRetencion(retencion);
		
	}
	
	

	public int getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(int horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	public String getNombre() {
		return nombre;
	}
	
	public int getHijos() {
		return hijos;
	}

	public Boolean getEsCasado() {
		return esCasado;
	}

	public void setEsCasado(Boolean esCasado) {
		this.esCasado = esCasado;
	}
	
	public void setHijos(int hijos) {
		this.hijos = hijos;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Double getSueldoBasico() {
		return sueldoBasico;
	}

	public void setSueldoBasico(Double sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}
	
	private void setRetencion(int retencion) {
		
		this.retencion = retencion;
		
	}
	
	protected Double getRetenciones() {
		return this.sueldo() * this.retencion;
	}
	
	protected abstract Double plusFamilia();
	protected abstract Double plushorasTrabajadas();
	
	public final Double sueldo() {
		
		return this.getSueldoBasico() + this.plushorasTrabajadas() + this.plusFamilia();
		
	}
	
		

}
