package ar.edu.unq.po2.tp7.SueldosRecargados;

public class EmpleadoPlanta extends Empleado {

	public EmpleadoPlanta(Double sueldoBasico, String nombre, String apellido, int hijos, Boolean esCasado, int horas, int retencion) {
		super(sueldoBasico, nombre, apellido, hijos, esCasado, horas, retencion);
		
		
	}



	@Override
	protected Double plusFamilia() {
				
		return this.getHijos() * 150d;
	}

	@Override
	protected Double plushorasTrabajadas() {
		
		return 0d;
	}

	
	
}
