package ar.edu.unq.po2.tp7.SueldosRecargados;

public class EmpleadoTemporario extends Empleado {
	
	
	public EmpleadoTemporario(Double sueldoBasico, String nombre, String apellido, int hijos, Boolean esCasado, int horas, int retencion) {
		super(sueldoBasico, nombre, apellido, hijos, esCasado, horas, retencion);
		
		
	}

	
	@Override
	protected Double plusFamilia() {
		Double plus = 0d;
		
			if (this.getEsCasado() || this.getHijos() > 0) {
				plus = 100d;
			}
		return plus;
	}

	@Override
	protected Double plushorasTrabajadas() {
		
		return this.getHorasTrabajadas() * 5d;
	}

}
