package ar.edu.unq.po2.tp7.SueldosRecargados;

public class EmpleadoPasante extends Empleado {
	
	

	public EmpleadoPasante(Double sueldoBasico, String nombre, String apellido, int hijos, Boolean esCasado, int horas, int retencion) {
		super(sueldoBasico, nombre, apellido, hijos, esCasado, horas, retencion);
		
		
	}

	@Override
	protected Double plusFamilia() {
		
		return 0d;
	}

	@Override
	protected Double plushorasTrabajadas() {
		
		return this.getHorasTrabajadas() * 40d;
	}

}
