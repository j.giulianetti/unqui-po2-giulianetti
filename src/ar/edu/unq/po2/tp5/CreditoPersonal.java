package ar.edu.unq.po2.tp5;

public class CreditoPersonal extends Credito {

	public CreditoPersonal(Cliente cliente, Double monto, int plazo) {
		super(cliente, monto, plazo);
		
	}

	public Boolean validarCuota(Cliente cliente, Double monto, int plazo) {
		
		return this.cuota(monto, plazo) < cliente.getSueldoNeto()*0.7;
	}
	
	public Boolean validarRequisitosPersonales(Cliente cliente, Double monto, int plazo) {
		
		return cliente.SueldoAnual()>=15000;
	}


}
