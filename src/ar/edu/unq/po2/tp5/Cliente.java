package ar.edu.unq.po2.tp5;

public class Cliente {
	
		
	private String nombre;
	private String apellido;
	private String direccion;
	private int edad;
	private Double sueldoNeto;
	private Propiedad propiedad;
	
	
	public Cliente(String nombre, String apellido, String direccion, int edad, Double sueldoNeto) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.edad = edad;
		this.sueldoNeto = sueldoNeto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public Double getSueldoNeto() {
		return sueldoNeto;
	}
	public void setSueldoNeto(Double sueldoNeto) {
		this.sueldoNeto = sueldoNeto;
	}
	
	public Double SueldoAnual() {
		return this.getSueldoNeto() * 12;
	}
	public Propiedad getPropiedad() {
		return propiedad;
	}
	public void setPropiedad(Propiedad propiedad) {
		this.propiedad = propiedad;
	}
	

}
