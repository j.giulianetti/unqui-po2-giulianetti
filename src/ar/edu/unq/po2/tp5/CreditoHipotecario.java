package ar.edu.unq.po2.tp5;

public class CreditoHipotecario extends Credito {

	public CreditoHipotecario(Cliente cliente, Double monto, int plazo) {
		super(cliente, monto, plazo);
		
	}

	
	public Boolean validarEdad(Cliente cliente, int plazo) {
		
		return cliente.getEdad() + plazo/12 < 65;
		
	}
	
	public Boolean validarPropiedad(Cliente cliente, Double monto) {
		
		return monto <= cliente.getPropiedad().getValorFiscal() * 0.7;
		
	}
	
	public Boolean validarCuota(Cliente cliente, Double monto, int plazo) {
		
		return this.cuota(monto, plazo) < cliente.getSueldoNeto()*0.5;
	}

	
	public Boolean validarRequisitosPersonales(Cliente cliente, Double monto, int plazo) {
				
		return this.validarEdad(cliente, plazo) && this.validarPropiedad(cliente, monto);
	}

}
