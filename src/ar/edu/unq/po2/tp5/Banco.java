package ar.edu.unq.po2.tp5;

import java.util.ArrayList;

public class Banco {
	
	private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	private String nombre;
	
	public Banco(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Cliente> getClientes() {
		return clientes;
	}

	public void addClientes(Cliente cliente) {
		this.clientes.add(cliente);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void solicitarCredito(Cliente cliente, Credito credito, Double monto, int plazo) {
		
		if (credito.validar(cliente, monto, plazo)) {
			credito.otorgar(cliente, monto, plazo);
		}
		
	}
	

}
