package ar.edu.unq.po2.tp5;

public abstract class Credito {
	
	private Cliente cliente;
	private Double monto;
	private int plazo;
	private int interes;
	
	public Credito(Cliente cliente, Double monto, int plazo) {
		this.cliente = cliente;
		this.monto = monto;
		this.plazo = plazo;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public int getPlazo() {
		return plazo;
	}
	public void setPlazo(int plazo) {
		this.plazo = plazo;
	}
	
	public double cuota(Double monto, int plazo) {
		
		return (monto + this.interes / plazo);
	}
	
	public abstract Boolean validarCuota (Cliente cliente, Double monto, int plazo);
	public abstract Boolean validarRequisitosPersonales(Cliente cliente, Double monto, int plazo);
	
	
	public final Boolean validar(Cliente cliente, Double monto, int plazo) {
		
		return this.validarCuota(cliente, monto, plazo) 
				&& this.validarRequisitosPersonales(cliente, monto, plazo);
		
	}
	
	public void otorgar(Cliente cliente, Double monto, int plazo) {
				
	}
	
}
