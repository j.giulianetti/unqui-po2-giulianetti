package ar.edu.unq.po2.tp8.Encriptador;

public class EncriptadorPalabrasANumeros implements Iencriptador {

	
	private String alfabeto = " abcdefghijklmnopqrstuvwxyz";
	
	public boolean mismaLetra(String letra1, String letra2) {
		return letra1 == letra2;
	}
	
	public String cambiarletra(String letra) {
		
		String mensajeNuevo = "";
		
		for (int i=0; i<this.alfabeto.split("").length;i++) {
			
			mensajeNuevo = mensajeNuevo + i;
			mensajeNuevo.concat(",");
		}	
		return mensajeNuevo;
	}
	
	@Override
	public String encriptar(String mensaje) {
		
		String mensajeNuevo = "";
		
		for (String letra : mensaje.split("")) {
			mensajeNuevo.concat(this.cambiarletra(letra));
		}
		return mensajeNuevo;
	}

	
	@Override
	public String desencriptar(String mensaje) {
		
		String mensajeNuevo = "";
		
		for (String clave : mensaje.split(",")) {
			mensajeNuevo = mensajeNuevo + this.alfabeto.split("")[Integer.parseInt(clave)];
		}
		return mensajeNuevo;
	}	
		

}
