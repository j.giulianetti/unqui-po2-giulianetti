package ar.edu.unq.po2.tp8.Encriptador;

public class Computadora {
	
	private Iencriptador estrategiaDeEncriptación;
	
	public Computadora(Iencriptador metodoDeEncriptación) {
		
		this.setEstrategiaDeEncriptación(metodoDeEncriptación);
		
	}
	public Iencriptador getEstrategiaDeEncriptación() {
		return this.estrategiaDeEncriptación;
	}
	public void setEstrategiaDeEncriptación(Iencriptador metodoDeEncriptación) {
		this.estrategiaDeEncriptación = metodoDeEncriptación;
	}
	
	
	private String encriptarMensaje(String mensaje) {
		
		return this.getEstrategiaDeEncriptación().encriptar(mensaje);
		
	}
	
	private String desEncriptarMensaje(String mensaje) {
		
		return this.getEstrategiaDeEncriptación().desencriptar(mensaje);
		
	}

}
