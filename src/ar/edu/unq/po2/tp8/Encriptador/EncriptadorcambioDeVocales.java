package ar.edu.unq.po2.tp8.Encriptador;


public class EncriptadorcambioDeVocales implements Iencriptador {

	private String mensajeEncriptado;
	private String mensajeDesencriptado;
	private String vocales = "aeiou";
	


private String getMensajeEncriptado() {
	return this.mensajeEncriptado;
}

private String getMensajeDesencriptado() {
	return this.mensajeDesencriptado;
}
	
	
public Boolean esVocal(String letra) {
	return this.vocales.contains(letra);
}
	
public void encriptarSiEsVocal(String letra) {

	String[] vocalAComparar = this.vocales.split("");
	String nuevaLetra = letra;
	
	if (this.esVocal(letra)) {
		for (int i = 0; i<5 ; i++) {
			if (letra == vocalAComparar[i]) {
				nuevaLetra = vocalAComparar[i+1];
			}
		}
	}
	this.getMensajeDesencriptado().concat(nuevaLetra);
}
	
public void desencriptarSiEsVocal(String letra) {
	
	String[] vocalAComparar = this.vocales.split("");
	String nuevaLetra = letra;

	if (this.esVocal(letra)) {
		for (int i = 0; i<5 ; i++) {
			if (letra == vocalAComparar[i]) {
				nuevaLetra = vocalAComparar[i-1];
			}
		}
	}
	this.getMensajeDesencriptado().concat(nuevaLetra);
}
	
@Override
public String encriptar(String mansajeOriginal) {

	for (String letra : mansajeOriginal.split("")) {
		this.encriptarSiEsVocal(letra);
	}
	return this.getMensajeEncriptado();
}

@Override
public String desencriptar(String mensajeEncriptado) {
				
	for (String letra : mensajeEncriptado.split("")) {
		this.desencriptarSiEsVocal(letra);
	}
	return this.getMensajeDesencriptado();
}
}








