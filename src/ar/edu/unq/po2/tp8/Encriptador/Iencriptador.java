package ar.edu.unq.po2.tp8.Encriptador;

public interface Iencriptador {
	
	public String encriptar(String mensaje);
	public String desencriptar(String mensaje);

}
