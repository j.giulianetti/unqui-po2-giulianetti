package ar.edu.unq.po2.tp8.ReproductorMP3;

public class EstadoReproduciendoCancion implements IestadoDeCanción {

	
	Reproductor reproductor;
	
	public EstadoReproduciendoCancion(Reproductor reproductor) {
		this.reproductor = reproductor;
	}

	@Override
	public void play() {
		
		//Mensaje de error 

	}

	@Override
	public void pause() {
		
		this.reproductor.pauseSong();
		this.reproductor.cambiarEstado(new EstadoDeCancionEnPausa(this.reproductor));

	}

	@Override
	public void stop() {
		
		this.reproductor.stopSong();
		this.reproductor.cambiarEstado(new EstadoSeleccionDeCanciones(reproductor));

	}

}
