package ar.edu.unq.po2.tp8.ReproductorMP3;

public class EstadoDeCancionEnPausa implements IestadoDeCanción {

	private Reproductor reproductor;
	
	
	
	public EstadoDeCancionEnPausa(Reproductor reproductor) {
		this.reproductor = reproductor;
	}

	@Override
	public void play() {
		
		//Mensaje de error

	}

	@Override
	public void pause() {
		
		this.reproductor.playSelectedSong();
		this.reproductor.cambiarEstado(new EstadoReproduciendoCancion(reproductor));
	}

	@Override
	public void stop() {
		
		this.reproductor.stopSong();
		this.reproductor.cambiarEstado(new EstadoSeleccionDeCanciones(reproductor));

	}

}
