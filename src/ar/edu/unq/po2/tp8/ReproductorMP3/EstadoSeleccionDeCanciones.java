package ar.edu.unq.po2.tp8.ReproductorMP3;


public class EstadoSeleccionDeCanciones implements IestadoDeCanción {

	private Reproductor reproductor;
	private String mensajeDeError;
	
	
	public EstadoSeleccionDeCanciones(Reproductor reproductor) {
		this.reproductor = reproductor;
	}

	@Override
	public void play() {
		
		this.reproductor.playSelectedSong();
		this.reproductor.cambiarEstado(new EstadoReproduciendoCancion(reproductor));

	}

	@Override
	public void pause() {
		
		//Mensaje de error

	}

	@Override
	public void stop() {
		// No hay comportamiento

	}

}
