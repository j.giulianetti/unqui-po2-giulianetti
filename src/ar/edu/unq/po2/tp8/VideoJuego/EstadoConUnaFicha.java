package ar.edu.unq.po2.tp8.VideoJuego;

public class EstadoConUnaFicha implements Iestado {

	
	Arcade arcade;
	
	public EstadoConUnaFicha(Arcade arcade) {
		this.arcade = arcade;
	}

	@Override
	public void ponerFicha(Arcade arcade) {
		
		this.arcade.cambiarEstado(new EstadoConDosFichas(arcade));

	}

	@Override
	public void presionarStart(Arcade arcade) {
		
		this.arcade.IniciarJuegoSimple();
		this.arcade.cambiarEstado(new EstadoSinFichas(arcade));

	}

}
