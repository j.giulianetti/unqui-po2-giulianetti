package ar.edu.unq.po2.tp8.VideoJuego;

public class EstadoConDosFichas implements Iestado {

	Arcade arcade;
		
	public EstadoConDosFichas(Arcade arcade) {
		this.arcade = arcade;
	}

	@Override
	public void ponerFicha(Arcade arcade) {
		
		this.arcade.devolverFicha();

	}

	@Override
	public void presionarStart(Arcade arcade) {
		
		this.arcade.iniciarJuegoDoble();
		this.arcade.cambiarEstado(new EstadoSinFichas(arcade));

	}

}
