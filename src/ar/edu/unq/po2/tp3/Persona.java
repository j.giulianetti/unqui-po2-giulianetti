package ar.edu.unq.po2.tp3;

import java.time.LocalDate;
import java.time.Period;
import java.util.Date;




public class Persona {
	
	private String nombre;
	private String apellido;
	private LocalDate fechaNacimiento;
	
	
	public Persona(String nombre, String apellido, LocalDate fechaNacimiento) {
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setfechaNacimiento(fechaNacimiento);
	}
	


	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getApellido() {
		return apellido;
	}



	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	public LocalDate getfechaNacimiento() {
		return fechaNacimiento;
	}



	public void setfechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}



    public int getEdad() {
     
            return 0;//Period.between(this.getfechaNacimiento(), now);
           
           
    }
	


	public Boolean menorQue(Persona persona) {
		return this.getEdad() < persona.getEdad();
	}
}
