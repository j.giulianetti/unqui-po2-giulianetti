package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class EquipoDeTrabajo {
	
	private String nombreEquipo;
	private ArrayList<Persona> equipo = new ArrayList<Persona>();
	
	
	public EquipoDeTrabajo(String nombre) {
		this.nombreEquipo = nombre;
		
	}
	
	
	public void addPersona(Persona persona) {
		this.equipo.add(persona);
	}
	
	public String getNombre() {
		return this.nombreEquipo;
	}
	
	public float edadPromedio() {
		
		Multioperador sumaDeEdades = new Multioperador();
		int i = 0;
		
		for (Persona integrante : this.equipo) {
			
			sumaDeEdades.addNumber(integrante.getEdad());
			i++;
		}
		
		return (sumaDeEdades.sumar() / i);
	}

}
