package ar.edu.unq.po2.tp3;

import java.util.ArrayList;


public class Counter {
	
	private ArrayList<Integer> lista = new ArrayList<Integer>();
	
	
	public ArrayList<Integer> getLista() {
		return lista;
	}

	public void addNumber(Integer n) {
		this.getLista().add(n);
	}


	public Integer contarPares() {
		
		int Pares = 0; 
		
		for (int n : this.lista) {
			
			if (n % 2 == 0 ) {
			Pares ++;
			}
			
		}
		
		return Pares;
		
	}


	public Integer contarImpares () {
		
		return this.getLista().size() - this.contarPares();
		
	}



	public Integer contarMultiplos(int m) {
		
		int Cantidad = 0; 
		
		for (int n : this.lista) {
		
			if (n % m == 0 ) {
			Cantidad ++;
			}
			
		}
		
		return Cantidad;
		
	}


}
