package ar.edu.unq.po2.tp3;

public class Punto {
	
	private int x;
	private int y;
	
	
	public Punto () {
		this.setxy(0, 0);
	}
	
	
	public Punto(int x, int y) {
		this.setxy(x, y);
	}
	
	
	public void setxy(int x, int y) {
	this.setX(x);
	this.setY(y);
	}
	

	public void moveXY(int x, int y) {
		this.setxy(x, y);
	}
	
	
	public Punto sumarPuntos(Punto otroPunto) {
		
		return new Punto (this.getX() + otroPunto.getX(),this.getY() + otroPunto.getY());
		
	}
	
	
	public int getY() {
		return this.y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	

	public int getX() {
		return this.x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	

}
