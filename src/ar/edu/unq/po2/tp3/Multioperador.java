package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class Multioperador {
	
	
	private ArrayList<Integer> lista = new ArrayList<Integer>();
	

	public ArrayList<Integer> getLista() {
		return lista;
	}

	public void addNumber(Integer n) {
		this.getLista().add(n);
	}
	
public int sumar() {
		
		int suma = 0;
		
		for (int n : this.getLista()) {
			
			suma = suma + n;
		}
	return suma;
}
	
	
	public int sumar(ArrayList<Integer> lista) {
		
		int suma = 0;
		
		for (int n : lista) {
			
			suma = suma + n;
		}
	return suma;
	}
	
	public int restar(ArrayList<Integer> lista) {
		
		int resta = 0;
		
		for (int n : lista) {
			
			resta = resta - n;
		}
	return resta;
	
	
	}
	
	public int multiplicar(ArrayList<Integer> lista) {
		
		int producto = 1;
		
		for (int n : lista) {
			
			producto = producto * n;
		}
	return producto;
	}
	
		
	
}
