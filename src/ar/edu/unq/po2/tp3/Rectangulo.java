package ar.edu.unq.po2.tp3;

public class Rectangulo {
	
	private int alto;
	private int ancho;
	private Punto ubicacion;
	
	
	public Rectangulo(int alto, int ancho, Punto ubicacion) {
		this.setAlto(alto);
		this.setAncho(ancho);
		this.setUbicacion(ubicacion);
	}
		
	
	public int getAlto() {
		return alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public void cambiarAlto(int nuevaAltura) {
		this.alto = nuevaAltura;
	}
	
	public void cambiarAncho(int nuevoAncho) {
		this.ancho = nuevoAncho;
	}

	public int area() {
		return this.ancho * this.alto;
	}

	public int perinmetro() {
		return this.ancho * 2 + this.alto * 2;
	}

	public Punto getUbicacion() {
		return this.ubicacion;
	}

	public void setUbicacion(Punto ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	public String getOrientacion() {
		
		String orientacion = "vertical";
		
		if (this.getAncho() > this.getAlto()) {
			
			orientacion = "horizontal";
		}
		
		return orientacion;
	}
}
