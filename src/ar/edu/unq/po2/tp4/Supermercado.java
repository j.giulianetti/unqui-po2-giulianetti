package ar.edu.unq.po2.tp4;

import java.util.ArrayList;

public class Supermercado {
	
	private String nombre;
	private String direccion;
	private ArrayList<Producto> Productos = new ArrayList<Producto>();
	
	
	public Supermercado(String nombre, String direccion) {
		this.setNombre(nombre);
		this.setDireccion(direccion);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public void agregarProducto(Producto p) {
		
		this.Productos.add(p);
		
	}
	
	
	public int getCantidadDeProductos() {
		
		return this.Productos.size();
		
	}
	
	public Double getPrecioTotal() {
		
		Double total=0.0;
		
		for (Producto p : this.Productos) {
			
			total = total + p.getPrecio();
			
		}
		
		return total;
		
	}

}
