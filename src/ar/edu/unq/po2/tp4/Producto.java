package ar.edu.unq.po2.tp4;

public class Producto {
	
	private String nombre;
	private Double precioBase;
	private Boolean preciosCuidados = false;
	
	
public Producto(String nombre, Double precio) {
		
		this.setNombre(nombre);
		this.setPrecio(precio);
		
	}
	
	
		
	public Producto(String nombre, Double precio, Boolean preciosCuidados) {
		
		this.nombre = nombre;
		this.precioBase = precio;
		this.setPreciosCuidados(preciosCuidados);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Double getPrecio() {
		return this.precioBase;
	}
	public void setPrecio(Double precio) {
		this.precioBase = precio;
	}
	public Boolean esPrecioCuidado() {
		return preciosCuidados;
	}
	public void setPreciosCuidados(Boolean preciosCuidados) {
		this.preciosCuidados = preciosCuidados;
	}

	public void aumentarPrecio(Double precio) {
		
		this.setPrecio( this.getPrecio() + precio);
		
	}



}
