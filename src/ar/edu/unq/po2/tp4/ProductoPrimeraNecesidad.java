package ar.edu.unq.po2.tp4;

public class ProductoPrimeraNecesidad extends Producto {

	private int descuento;
	
	public ProductoPrimeraNecesidad(String nombre, Double precio, int descuento) {
		super(nombre, precio);
		this.setDescuento(descuento);
		
	}
	
	
	public ProductoPrimeraNecesidad(String nombre, Double precio, Boolean preciosCuidados) {
		super(nombre, precio, preciosCuidados);
		
	}

	public void setDescuento(int descuento) {
		this.descuento=descuento;
	}
	
	public int getDescuento() {
		return this.descuento;
	}
	
	public Double getPrecio() {
		
		return ((this.getPrecio() * this.getDescuento())/100);
		
	}
	
}
