package ar.edu.unq.po2.tp4;

public abstract class Ingreso {
	
	private String mes;
	private String concepto;
	private Double monto;
	private Boolean esImponible;
	
	
	public Ingreso(String mes, String concepto, Double monto, Boolean esImponible) {
		this.setMes(mes);
		this.setConcepto(concepto);
		this.setMonto(monto);
		this.setEsImponible(esImponible);
	}
	
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Boolean getEsImponible() {
		return esImponible;
	}

	public void setEsImponible(Boolean esImponible) {
		this.esImponible = esImponible;
	}
	
	

}
