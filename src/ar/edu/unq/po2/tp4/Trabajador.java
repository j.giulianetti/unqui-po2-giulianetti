package ar.edu.unq.po2.tp4;

import java.util.ArrayList;


public class Trabajador {
	
	private ArrayList<Ingreso> ingresos = new ArrayList<Ingreso>();
	
	public void registrarIngreso(Ingreso ingreso) {
		this.ingresos.add(ingreso);
	}
	
	public ArrayList<Ingreso> getIngresos(){
		return this.ingresos;
	}
	
	
	public Double getTotalPercibido() {
		
		Double total = 0d;
		
		for(Ingreso i : this.getIngresos()) {
			
			total = total +i.getMonto();
		}
		
		return total;
	}
	
	
	public Double getMontoImponible() {
		
		Double totalImponible=0d;
		
		for (Ingreso i : ingresos) {
			
			if (i.getEsImponible()) {
				totalImponible = totalImponible + i.getMonto();
			}
			
		}
		
		return totalImponible;
	}
	
	
	public Double getImpuestoAPagar() {
		return this.getMontoImponible() * 0.2;
	}

}
