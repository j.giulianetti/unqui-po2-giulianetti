package ar.edu.unq.po2.tp4;

public class IngresoPorHorasExtra extends Ingreso {
	
	private int horasExtra;

	public IngresoPorHorasExtra(String mes, String concepto, Double monto, Boolean esImponible, int horasExtra) {
		super(mes, concepto, monto, esImponible);
		this.setHorasExtra(horasExtra);
	}

	public int getHorasExtra() {
		return horasExtra;
	}

	public void setHorasExtra(int horasExtra) {
		this.horasExtra = horasExtra;
	}
	
	public Double getMonto() {
		return super.getMonto() * this.getHorasExtra();
	}

}
