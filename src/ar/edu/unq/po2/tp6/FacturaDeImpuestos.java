package ar.edu.unq.po2.tp6;

public class FacturaDeImpuestos extends Factura {
	
	private Double tasa;
	
	
	public FacturaDeImpuestos(Double tasa) {
		super();
		this.setTasa(tasa);
	}

	public Double getTasa() {
		return tasa;
	}

	public void setTasa(Double tasa) {
		this.tasa = tasa;
	}

	
	public Double getPrecio() {
		
		return this.getTasa();
	}
	
	

}
