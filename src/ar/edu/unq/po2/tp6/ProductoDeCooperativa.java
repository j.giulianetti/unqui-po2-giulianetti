package ar.edu.unq.po2.tp6;

public class ProductoDeCooperativa extends Producto implements Cobrable {

	public ProductoDeCooperativa(Double precio, int stock, String nombre) {
		super(precio, stock, nombre);
		
	}

	public Double getPrecio() {
		return (super.getPrecio() * 0.9) ;
	}
}
