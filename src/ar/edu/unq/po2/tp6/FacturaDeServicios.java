package ar.edu.unq.po2.tp6;

public class FacturaDeServicios extends Factura {
	
	private Double costoUnidad;
	private int unidadesConsumidas;
	
	
	
	public FacturaDeServicios(Double costoUnidad, int unidadesConsumidas) {
		super();
		this.setCostoUnidad(costoUnidad);
		this.setUnidadesConsumidas(unidadesConsumidas);
	}

	public int getUnidadesConsumidas() {
		return unidadesConsumidas;
	}

	public void setUnidadesConsumidas(int unidadesConsumidas) {
		this.unidadesConsumidas = unidadesConsumidas;
	}

	public Double getCostoUnidad() {
		return costoUnidad;
	}

	public void setCostoUnidad(Double costoUnidad) {
		this.costoUnidad = costoUnidad;
	}

	
	public Double getPrecio() {
		
		return this.getUnidadesConsumidas() * this.getCostoUnidad();
	}
	

}
