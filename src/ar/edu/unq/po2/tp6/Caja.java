package ar.edu.unq.po2.tp6;

import java.util.ArrayList;
import java.util.List;



public class Caja {
	
	private List<Cobrable> Productos = new ArrayList<Cobrable>();

	
	public List<Cobrable> getProductos() {
		return Productos;
	}

	public void registrarProducto(Cobrable producto) {
		Productos.add(producto);
	}
	
	public Double realizarCompra() {
		
		Double montoAPagar=0d;
		
		for (Cobrable producto : this.getProductos()) {
			montoAPagar = montoAPagar + producto.getPrecio();
			producto.concretarCompra();
		}
		return montoAPagar;
	}

	
}
