package ar.edu.unq.po2.tp6;

public abstract class Producto implements Cobrable{

	
	private Double precio;
	private int stock;
	private String nombre;

	public Producto(Double precio, int stock, String nombre) {
		this.precio = precio;
		this.stock = stock;
		this.nombre = nombre;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public void decrementarStock() {
		this.stock--;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public  void concretarCompra() {
		
		this.stock --;
	}
	
	
}
