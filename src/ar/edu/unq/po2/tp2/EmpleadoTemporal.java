package ar.edu.unq.po2.tp2;

public class EmpleadoTemporal extends Empleado {
	
	private Integer horasExtra;
	private Integer finContrato;
	
	
	
	public EmpleadoTemporal(String nombre, String dirección, Boolean escasado, Double sueldoBasico,
			Integer fechaNacimiento, Integer horasExtra, Integer finContrato) {
		super(nombre, dirección, escasado, sueldoBasico, fechaNacimiento);
		this.setHorasExtra(horasExtra);
		this.setFinContrato(finContrato);
		
	
		
	}
	
	
	public Integer getHorasExtra() {
		return this.horasExtra;
	}
	public void setHorasExtra(Integer horasExtra) {
		this.horasExtra = horasExtra;
	}
	public Integer getFinContrato() {
		return this.finContrato;
	}
	public void setFinContrato(Integer finContrato) {
		this.finContrato = finContrato;
	}

	@Override
	public Double getAporteObraSocial() {
		
		int mayorDe50 = 0;
		
		if (this.getEdad()>50) {
			mayorDe50 = 25;
		}
		
	
		return this.calcularSueldoBruto() * 0.1 + mayorDe50;
	}

	@Override
	public Double getAporteJubilacion() {
		
		return this.calcularSueldoBruto() * 0.1 + this.getHorasExtra() * 5;
	} 

	@Override
	public Double calcularSueldoBruto() {

		return this.getSueldoBasico() + this.getHorasExtra() * 40;

	}

	
	




	
	
	
	
	

}
