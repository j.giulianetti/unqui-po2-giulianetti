package ar.edu.unq.po2.tp2;

import java.util.ArrayList;

public class Empresa {
	
	private String nombre;
	private Integer cuit;
	private ArrayList<Recibo> recibos = new ArrayList<Recibo>();
	private ArrayList<Empleado> nomina = new ArrayList<Empleado>();
	
	
	public Empresa(String nombre, Integer cUIT) {
		
		this.nombre = nombre;
		this.cuit = cUIT;
		
	}
	
	
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getCUIT() {
		return this.cuit;
	}
	public void setCUIT(Integer cuit) {
		this.cuit = cuit;
	}
	
	public ArrayList<Empleado> getNomina() {
		return this.nomina;
	}
	public void addEmpleado(Empleado empleado) {
		this.nomina.add(empleado);
	}
	
	public ArrayList<Recibo> getRecibos() {
		return this.recibos;
	}
	public void archivarRecibo(Recibo recibo) {
		this.recibos.add(recibo);
	}
	
	
	
	public Double getTotalNeto() {
		
		Double totalNeto = 0d;
		
		for (Empleado empleado : this.getNomina()) {
			totalNeto = totalNeto + empleado.calcularSueldoNeto();
		}
		return totalNeto;
	}
	
	public Double getTotalBruto() {
		
		Double totalBruto = 0d;
		
		for (Empleado empleado : this.getNomina()) {
			totalBruto = totalBruto + empleado.calcularSueldoBruto();
		}
		return totalBruto;
	}
	
	public Double getTotalRetenciones(){
		
		Double totalRetenciones = 0d;
		
		for (Empleado empleado : this.getNomina()) {
			totalRetenciones = totalRetenciones + empleado.calcularRetenciones();
		}
		return totalRetenciones;
	}
	
		
	public void liquidarSuedos(){
		
		
		for (Empleado empleado : this.getNomina()) {
			this.archivarRecibo(new Recibo(empleado));
		}
		
		
		
		
	}
	
	
	
}
