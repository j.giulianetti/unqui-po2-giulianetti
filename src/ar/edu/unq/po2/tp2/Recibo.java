package ar.edu.unq.po2.tp2;

import java.util.Date;

public class Recibo {
	
	private String nombre;
	private String direccion;
	private Date fechaEmision;
	private Double sueldoBruto;
	private Double sueldoNeto;
	
	
	public Recibo (Empleado empleado) {
		
		this.setNombre(empleado.getNombre());
		this.setDireccion(empleado.getDireccion());
		//this.fechaEmision = today;
		this.setSueldoBruto(empleado.calcularSueldoBruto());
		this.setSueldoNeto(empleado.calcularSueldoNeto());
		
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public Date getFechaEmision() {
		return fechaEmision;
	}


	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}


	public Double getSueldoBruto() {
		return sueldoBruto;
	}


	public void setSueldoBruto(Double sueldoBruto) {
		this.sueldoBruto = sueldoBruto;
	}


	public Double getSueldoNeto() {
		return sueldoNeto;
	}


	public void setSueldoNeto(Double sueldoNeto) {
		this.sueldoNeto = sueldoNeto;
	}

	
	
}
