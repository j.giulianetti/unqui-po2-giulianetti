package ar.edu.unq.po2.tp2;

public class EmpleadoPermanente extends Empleado {
	
	
	private Integer cantHijos;
	private Integer antiguedad;
	
	
	public EmpleadoPermanente(String nombre, String dirección, Boolean escasado, Double sueldoBasico,
			Integer fechaNacimiento, Integer cantHijos, Integer antiguedad) {
		
		super(nombre, dirección, escasado, sueldoBasico, fechaNacimiento);
		
		this.setCantHijos(cantHijos);
		this.setAntiguedad(antiguedad);
	
	
		
	}
	
	
	public Integer getCantHijos() {
		return this.cantHijos;
	}
	public void setCantHijos(Integer cantHijos) {
		this.cantHijos = cantHijos;
	}
	public Integer calcularAntiguedad() {
		return this.antiguedad * 50;
	}
	public void setAntiguedad(Integer antiguedad) {
		this.antiguedad = antiguedad;
	}
	
	public Double calcularSalarioFamiliar() {
		
		Double plusConyuge = 0d;
		
		if (this.getEscasado()){
			plusConyuge = 100d;
			}
		
		return this.getCantHijos() * 150 + plusConyuge;
	}
		
	@Override
	public Double getAporteObraSocial() {
		
		return this.calcularSueldoBruto() * 0.1 + this.getCantHijos() * 20;
	}


	@Override
	public Double getAporteJubilacion() {
		
		return this.calcularSueldoBruto() * 0.15;
	}
	

	
	@Override
	public Double calcularSueldoBruto() {
		
		Double total;
		
		total = this.getSueldoBasico() 
				+ this.calcularSalarioFamiliar() 
					+ this.calcularAntiguedad();
		
		
		return total;
	}


	

}












