package ar.edu.unq.po2.tp2;

public class EmpleadoContratado extends Empleado {
	
	private Integer numeroContrato;
	private String metodoDePago;
	
		
	public EmpleadoContratado(String nombre, String dirección, Boolean escasado, Double sueldoBasico,
			Integer fechaNacimiento, Integer numeroContrato, String metodoDePago) {
		super(nombre, dirección, escasado, sueldoBasico, fechaNacimiento);
		setNumeroContrato(numeroContrato);
		setMetodoDePago(metodoDePago);
	
		
	
	}
	
	
	public Integer getNumeroContrato() {
		return this.numeroContrato;
	}
	public void setNumeroContrato(Integer numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getMetodoDePago() {
		return this.metodoDePago;
	}
	public void setMetodoDePago(String metodoDePago) {
		this.metodoDePago = metodoDePago;
	}


	@Override
	public Double calcularSueldoBruto() {
		
		return this.getSueldoBasico();
	}


	@Override
	public Double getAporteObraSocial() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Double getAporteJubilacion() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Double calcularRetenciones() {
		
		return 50d;
	
	}
	
}
