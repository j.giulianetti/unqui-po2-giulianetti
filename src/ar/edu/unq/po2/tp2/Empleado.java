package ar.edu.unq.po2.tp2;

public abstract class Empleado {

	private String nombre;
	private String direccion;
	private Boolean esCasado;
	private Double sueldoBasico;
	private Integer fechaNacimiento;

	
	
	public Empleado(String nombre, String direccion, Boolean escasado, Double sueldoBasico, Integer fechaNacimiento) {
		this.setNombre(nombre);
		this.setDireccion(direccion);
		this.setEscasado(escasado);
		this.setSueldoBasico(sueldoBasico);
		this.setFechaNacimiento(fechaNacimiento);
			
	}
	
	
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return this.direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Boolean getEscasado() {
		return this.esCasado;
	}
	public void setEscasado(Boolean escasado) {
		this.esCasado = escasado;
	}
	public Double getSueldoBasico() {
		return this.sueldoBasico;
	}
	public void setSueldoBasico(Double sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}
	public Integer getFechaNacimiento() {
		return this.fechaNacimiento;
	}
	public void setFechaNacimiento(Integer fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public Integer getEdad() {
		
		//Fecha de nacimiento - fecha actual
	return 0;
	}
	
	
	public abstract Double calcularSueldoBruto();
	public abstract Double getAporteObraSocial();
	public abstract Double getAporteJubilacion();
	
	
	public Double calcularRetenciones() {
	
		return this.getAporteObraSocial() + this.getAporteJubilacion();
	
	}
	
	public Double calcularSueldoNeto() {
		
		return this.calcularSueldoBruto() - this.calcularRetenciones();
	}


	
	
	



}
	
	
	
