import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp3.Counter;

public class CounterTestCase {

	Counter contador = new Counter();

@BeforeAll
public void setUp() throws Exception {



	contador.addNumber(1);
	contador.addNumber(3);
	contador.addNumber(5);
	contador.addNumber(7);
	contador.addNumber(9);
	contador.addNumber(1);
	contador.addNumber(1);
	contador.addNumber(1);
	contador.addNumber(1);
	contador.addNumber(4);

}

@Test

public void testEvenNumbers() {
// Getting the even occurrences
int amount = contador.contarPares();
// I check the amount is the expected one
assertEquals(amount, 1);
}
}